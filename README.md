SCIGR - Switch Configs Into Git Repository
==

A really light-weight alternative to RANCID, written in BASH.

Logs onto switches over ssh and runs "show running-config", captures and filters the output and then submits to a GIT repo.

I wrote this due to problems getting RANCID to work with our IBM/BNT G8052 switches, whose command language isn't /exactly/ like CISCO IOS, so the standard RANCID script won't work. There *is* a RANCID module that's supposed to work sometimes, but doesn't without config.

However, simply logging onto the switch with SSH and issuing:

    enable
    show running-config

*does* dump the config...

So this script is a simple wrapper around the sshpass and ssh commands, which logs into a list of switches and captures and filters the output from the "show running-config" command, then submits it to a GIT repository.

We have GITWEB installed and running, so that makes it available thorugh a nice web GUI.

It also emails out alerts when it detects a config has changed.

You can run it manually from the command-line or schedule it via cron.

It should run on any Unix-like OS, but was written on Debian Wheezy. You may have to tweak the find, grep and sed commands if you're using non-GNU versions of these.

If you want to run this on Windows - good luck, you might get some mileage out of CYGWIN.

Dependencies:

  *  GNU BASH
  *  GNU find
  *  GNU grep
  *  GNU sed
  *  pgrep
  *  ssh
  *  sshpass
  *  git
  *  diff
  *  Mutt

-Andy
