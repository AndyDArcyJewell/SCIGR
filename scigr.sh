#!/bin/bash

# cigr.sh 

# login to a list of switches over ssh, run "show running-config" and save to a
# file, then commit it to git repo

BASEDIR=/var/cache/git/switch_config/
HOSTFILE=$BASEDIR/hostfile.lst
RECIPIENTS="networguy@example.com"
INTRO="The following changes were made to SIG switch configs:\n\n"
SIG="\n\nregards,\n-$0"

# Have to blank out password hashes in config lines like this as they change every time you do show run:
# access user administrator-password "9f7bba2c8e78aa28b632f3e74dc809da91eb205ca504a3a8f5ae67145db29633"
# access user 1 password "9f7bba2c8e78aa28b632f3e74dc809da91eb205ca504a3a8f5ae67145db29633"

# Get details from host file table and process
grep -v "^#" $HOSTFILE | \
	while read target ip username pass subdir commands; do
		echo "target: $target ip: $ip username: $username pass: $pass subdir: $subdir"
		( sshpass -p "$pass" ssh -o StrictHostKeyChecking=no $username@$ip "$commands" | \
		sed 's/password "[^"]*"/password "*"/' | tee /tmp/$target.debug |\
		sed -n '/^Current configuration:/,/^end$/{ /^Current configuration:/d; /^end$/d; p; }' \
        > $BASEDIR$subdir/$target.conf.new  ) &
	done 

# wait for child processes to finish
while pgrep sshpass > /dev/null ; do
	sleep 1
done

added=""
changed=""
# Compare new and old configs and update repo copy if necessary
for new_one in `find $BASEDIR -name '*.conf.new'`; do
    old_one=${new_one%%.new}
#    echo "\$old_one: $old_one \$new_one: $new_one"

    # Make sure new file isn't zero-length
    if [[ -s $new_one ]]; then 

        # If its an entirely new file, append to added list
        if [[ ! -f $old_one ]]; then
            added="$added ${old_one##$basedir}"
        fi

        # If new is different from old
        diff=`diff -q $old_one $new_one` 
#        echo "\$diff: $diff"
        if [[ "${diff%%differ}" != "$diff" ]]; then
            echo "copying new to old"
            cp $new_one $old_one
            changed="$changed ${old_one##$BASEDIR}"
        fi        
    fi
    rm $new_one
        
done


msg=""
if [[ "$added" != "" ]]; then
    msg="$msg Added: $added "    
fi

if [[ "$changed" != "" ]]; then
    msg="$msg Changed: $changed"
fi

# If anything has changed, commit to git and email $RECIPIENTS to let them know
if [[ "$msg" != "" ]]; then
    echo "Added: $added"
    echo "changed: $changed"
    cd $BASEDIR
    git add .
    git status
    git commit -m "`date` $msg"
    echo -e "$INTRO `git diff HEAD^ HEAD` $SIG" | mutt -s "SCIGR Switch Config Changes: $msg" $RECIPIENTS
fi
